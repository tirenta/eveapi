﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using ConsoleChatClient.ProxyChat;

namespace ConsoleChatClient
{
    class Program
    {
        static void Main ( string [ ] args )
        {
            Console.Title = "T-Chat";
            Console.WriteLine ( "Введите свое имя" );
            string name = Console.ReadLine ( );
            MessageCallBack myMessage = new MessageCallBack ( name );
            InstanceContext context = new InstanceContext ( myMessage );

            ChatInterfaceClient client = new ChatInterfaceClient ( context );
            client.Open ( );
            client.NewUser ( name );

            string chatText = null;

            while ( true )
            {
                chatText = Console.ReadLine ( );

                if ( chatText != "/q" )
                {
                    client.NewMessage ( name , chatText );
                }
                else
                {
                    break;
                }
            }

            client.LeaveUser ( name );
            client.Close ( );
        }
    }

    public class MessageCallBack : IChatInterfaceCallback
    {
        private string name;
        public MessageCallBack ( string myName )
        {
            name = myName;
        }

        #region Члены IChatInterfaceCallback

        public void NewMessageCallBack ( string user , string message )
        {
            if ( name != user )
            {
                Console.WriteLine ( "{0}: {1}", user, message );
            }
        }

        public void NewUserCallBack ( string user )
        {
            Console.WriteLine ( "{0} зашел в чат", user );
        }

        public void LeaveUserCallBack ( string user )
        {
            Console.WriteLine ( "{0} вышел из чата" , user );
        }

        #endregion
    }
}
