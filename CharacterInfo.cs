﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EveApp.Interface;
using System.Xml;

namespace EveApp.Models
{
    [Serializable]
    public sealed class CharacterInfo : IUpdatable
    {
        #region Поля

        #region Основная информация персонажа

        int _characterID;
        string _characterName;

        int _corporationID;
        string _corporationName;

        int _allianceID;
        string _allianceName;

        int _factionID;
        string _factionName;

        #endregion

        #region Информация Апи

        CharacterApi _api;

        int _accessMask;
        string _type;
        DateTime _expires;

        #endregion

        #region Прочее

        DateTime _cachedUntil;

        #endregion

        #endregion

        #region Свойства

        //---------------------------------------
        public int CharacterID
        {
            get { return _characterID; }
            private set { _characterID = value; }
        }
        public string CharacterName
        {
            get { return _characterName; }
            private set { _characterName = value; }
        }

        //---------------------------------------
        public int CorporationID
        {
            get { return _corporationID; }
            private set { _corporationID = value; }
        }
        public string CorporationName
        {
            get { return _corporationName; }
            private set { _corporationName = value; }
        }
        
        //---------------------------------------
        public int AllianceID
        {
            get { return _allianceID; }
            private set { _allianceID = value; }
        }
        public string AllianceName
        {
            get { return _allianceName; }
            private set { _allianceName = value; }
        }

        //---------------------------------------
        public int FactionID
        {
            get { return _factionID; }
            private set { _factionID = value; }
        }
        public string FactionName
        {
            get { return _factionName; }
            private set { _factionName = value; }
        }

        //---------------------------------------
        public DateTime Expires
        {
            get { return _expires; }
            private set { _expires = value; }
        }

        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="api"></param>
        public CharacterInfo ( CharacterApi api )
        {
            _api = api;
            //UpdateFields ( );
        }
        public CharacterInfo ( ) { }

        #region Методы

        /// <summary>
        /// Парсит атрибуты xml файла апи
        /// и сортирует их по полям.
        /// </summary>
        /// <param name="sender">Поток XmlReader
        /// на узле с атрибутами</param>
        void ParseApiAttributes ( XmlReader sender )
        {
            if ( sender.Name == "key" )
            {
                for ( int i = 0 ; i < sender.AttributeCount ; i++ )
                {
                    sender.MoveToNextAttribute ( );

                    switch ( sender.Name )
                    {
                        case "accessMask":
                            _accessMask = Convert.ToInt32 ( sender.Value );
                            break;
                        case "type":
                            _type = !string.IsNullOrEmpty ( sender.Value ) ?
                                sender.Value : null;
                            break;
                        case "expires":
                            _expires = !string.IsNullOrEmpty ( sender.Value ) ?
                                Convert.ToDateTime ( sender.Value ) : _expires;
                            break;
                        default:
                            break;
                    }
                }
            }
            else if ( sender.Name == "row" )
            {
                for ( int i = 0 ; i < sender.AttributeCount ; i++ )
                {
                    sender.MoveToNextAttribute ( );

                    switch ( sender.Name )
                    {
                        case "characterID":
                            _characterID = Convert.ToInt32 ( sender.Value );
                            break;
                        case "characterName":
                            _characterName = !string.IsNullOrEmpty ( sender.Value ) ?
                                sender.Value : null;
                            break;
                        case "corporationID":
                            _corporationID = Convert.ToInt32 ( sender.Value );
                            break;
                        case "corporationName":
                            _corporationName = !string.IsNullOrEmpty ( sender.Value ) ?
                                sender.Value : null;
                            break;
                        case "allianceID":
                            _allianceID = Convert.ToInt32 ( sender.Value );
                            break;
                        case "allianceName":
                            _allianceName = !string.IsNullOrEmpty ( sender.Value ) ?
                                sender.Value : null;
                            break;
                        case "factionID":
                            _factionID = Convert.ToInt32 ( sender.Value );
                            break;
                        case "factionName":
                            _factionName = !string.IsNullOrEmpty ( sender.Value ) ?
                                sender.Value : null;
                            break;
                        default:
                            break;
                    }
                }
            }
            else
            {
                throw new ArgumentException ( "Недопустимый аргумент" );
            }
        }

        #endregion

        #region Члены IUpdatable

        public void UpdateFields ( )
        {
            using ( XmlReader reader = XmlReader.Create ( _api.GetUri ( "/Account" , "/APIKeyInfo" ) ) )
            {
                while ( !reader.EOF )
                {
                    if ( reader.MoveToContent ( ) == XmlNodeType.Element )
                    {
                        switch ( reader.Name )
                        {
                            case "key":
                                ParseApiAttributes ( reader );
                                reader.Read ( );
                                break;
                            case "row":
                                ParseApiAttributes ( reader );
                                reader.Read ( );
                                break;
                            case "cachedUntil":
                                Console.WriteLine ( reader.ReadElementContentAsString ( ) );
                                reader.Read ( );
                                break;
                            default:
                                reader.Read ( );
                                break;
                        }
                    }
                    else
                    {
                        reader.Read ( );
                    }
                }
            }
        }

        public bool CanUpdate ( )
        {
            return _cachedUntil <= DateTime.UtcNow ? true : false;
        }

        #endregion
    }
}