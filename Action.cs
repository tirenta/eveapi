﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

namespace LR
{
    public sealed class Autorization
    {
        //Константы
        const string _Host = "login.eveonline.com";

        //Поля
        string _Login, _Password, _Eula, _Token = null;
        CookieContainer cooks = new CookieContainer();
        string _loginAction =
            "ReturnUrl=%2Foauth%2Fauthorize%2F%3Fclient_id%3DeveLauncherTQ%26lang%3Den%26response_type%3Dtoken%" +
            "26redirect_uri%3Dhttps%3A%2F%2Flogin.eveonline.com%2Flauncher%3Fclient_id%" +
            "3DeveLauncherTQ%26scope%3DeveClientToken";
        bool _isToken = false;

        //Свойства
        public bool isValide { get; private set; }
        public bool isAccept { get; private set; }

        //Конструктор
        /// <summary>
        /// Сборка класса
        /// С запросом на получение куки
        /// И обновлением полей
        /// </summary>
        /// <param name="login">логин</param>
        /// <param name="password">пароль</param>
        public Autorization(string login, string password)
        {
            _Login = login;
            _Password = password;
            isValide = false;
            isAccept = true;
            GetCookie();
        }

        //Методы:

        /// <summary>
        /// Отправка логина и пароля
        /// </summary>
        public void Log_in()
        {
            string addr = string.Format("https://{0}/Account/LogOn?{1}",
                _Host, _loginAction);
            string logpas = string.Format("UserName={0}&Password={1}",
                _Login, _Password);
            _Request(addr, logpas);
        }

        /// <summary>
        /// Получение конечного токена
        /// для запуска игры
        /// </summary>
        /// <returns>закрытое поле _Token</returns>
        public string Get_Token()
        {
            if (!_isToken)
            {
                string addr = string.Format("https://{0}{1}",
                _Host, _loginAction);
                _Request(addr, null);
                if (isAccept)
                {
                    string pattern = @"#access_token=([^&]+)";
                    Match mat = Regex.Match(_loginAction, pattern);
                    if (mat.Success)
                    {
                        string token = mat.Groups[1].Value;
                        string _addr = string.Format("https://{0}/launcher/token?accesstoken={1}",
                            _Host, token);
                        _Request(_addr, null);
                        mat = Regex.Match(_loginAction, pattern);
                        if (mat.Success)
                        {
                            _Token = mat.Groups[1].Value;
                            _isToken = true;
                        }
                    }
                }//end if
            }//end if
            return _Token;
        }//end Get_Token

        /// <summary>
        /// Подтверждение Еула соглашения
        /// </summary>
        public void Eula_Accept()
        {
            string pat = "(.+)/oauth";
            string addr = string.Format("https://{0}/OAuth/Eula",
                _Host);
            _Request(addr, _Eula);
            isAccept = true;
            Regex rgx = new Regex(pat);
            _loginAction = rgx.Replace(_loginAction, "/oauth");
        }

        //Закрытые
        /// <summary>
        /// Получение куки с домена
        /// </summary>
        void GetCookie()
        {
            string urlFormat = string.Format("https://{0}/Account/LogOn?", _Host);
            HttpWebRequest hRequest = (HttpWebRequest)WebRequest.Create(urlFormat);
            hRequest.CookieContainer = cooks;
            hRequest.GetResponse();
        }
        /// <summary>
        /// Отправка запроса post/get
        /// Возвращение заголовка Location в поле
        /// </summary>
        /// <param name="url">адрес запроса</param>
        /// <param name="data">данные для запроса, null если нужен Get</param>
        void _Request(string url, string data)
        {
            string title = @"<title>(.+)</title>";
            HttpWebRequest hRequest = (HttpWebRequest)WebRequest.Create(url);
            hRequest.CookieContainer = cooks;
            hRequest.Host = _Host;
            hRequest.ContentType = "application/x-www-form-urlencoded";
            hRequest.AllowAutoRedirect = false;

            if (data != null)
            {
                hRequest.Method = "POST";
                byte[] byteArray = Encoding.UTF8.GetBytes(data);
                hRequest.ContentLength = byteArray.Length;
                Stream _dataStream = hRequest.GetRequestStream();
                _dataStream.Write(byteArray, 0, byteArray.Length);
                _dataStream.Close();
            }

            HttpWebResponse hResponse = (HttpWebResponse)hRequest.GetResponse();
            Stream dataStream = hResponse.GetResponseStream();

            StreamReader reader = new StreamReader(dataStream);
            string html = reader.ReadToEnd();
            reader.Close();
            dataStream.Close();

            for (int i = 0; i < hResponse.Headers.Count; i++)
            {
                if (hResponse.Headers.Keys[i] == "Location")
                {
                    _loginAction = hResponse.Headers[i];
                    isValide = true;
                    break;
                }
            }
            hResponse.Close();

            Match match = Regex.Match(html, title);
            if (match.Success)
            {
                switch (match.Groups[1].Value)
                {
                    case "License Agreement Update":
                        isAccept = false;
                        break;
                }
            }
        }//end _Request

        /// <summary>
        /// Поиск нужной информации для пост-запроса
        /// </summary>
        /// <param name="text">html страница в которой идет поиск</param>
        void GetEula(string text)
        {
            string[] names = new string[2];
            string[] values = new string[2];
            string search = @"<form action=.+/>";

            Match match = Regex.Match(text, search);
            if (match.Success)
            {
                string value = @"value=""([\w\d\s=""/:?;&.]+)""";
                string name = @"name=""([\w\S]+)""";

                Regex rgx = new Regex(name);
                MatchCollection mCol = rgx.Matches(match.Groups[1].Value);
                if (mCol.Count == 2)
                {
                    //names = new string[mCol.Count];
                    int _index = 0;
                    foreach (Match _m in mCol)
                    {
                        names[_index] = _m.Groups[1].Value;
                        _index++;
                    }
                }
                rgx = new Regex(value);
                mCol = rgx.Matches(match.Groups[1].Value);
                if (mCol.Count == 2)
                {
                    //values = new string[mCol.Count];
                    int _index = 0;
                    foreach (Match _m in mCol)
                    {
                        values[_index] = _m.Groups[1].Value;
                        _index++;
                    }
                }

                _Eula = string.Format("{0}={1}&{2}={3}",
                names[1], values[1], names[2], values[2]);
            }//end if
        }//end GetEula
    }
}
