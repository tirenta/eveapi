﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace EveApi
{
    sealed class ServerStatus : RequestApi
    {
        //Поля
        private bool _serverOpen;
        private int _onlinePlayers;

        //Переопределение
        protected override string SetLocation()
        {
            return "Server";
        }
        protected override string SetOption()
        {
            return "ServerStatus";
        }
        protected override void UpdateData(XDocument xml)
        {
            _serverOpen = Convert.ToBoolean(
                xml.Descendants("serverOpen").First().Value);
            _onlinePlayers = Convert.ToInt32(
                xml.Descendants("onlinePlayers").First().Value);
        }

        //Свойства
        public bool serverOpen
        {
            get
            {
                if (isObsolbete() == true)
                {
                    Update();
                    return _serverOpen;
                }
                else
                {
                    return _serverOpen;
                }
            }
        }
        public int onlinePlayers
        {
            get
            {
                if (isObsolbete() == true)
                {
                    Update();
                    return _onlinePlayers;
                }
                else
                {
                    return _onlinePlayers;
                }
            }
        }
    }//ServerStatus
}