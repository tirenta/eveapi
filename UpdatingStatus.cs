﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eve_gadget.Model;
using System.ComponentModel;
using System.Xml;
using System.Threading;

namespace Eve_gadget.ViewModel
{
    public class UpdatingStatus : INotifyPropertyChanged
    {
        StatusServer _status;
        TimeSpan _interval;

        public StatusServer Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
                //Уведомляем приложение о том,
                //что свойство изменилось.
                OnPropertyChanged ( "Status" );
            }
        }

        /// <summary>
        /// Конструктор класса.
        /// Инициализирует поля
        /// и фоновый поток.
        /// </summary>
        public UpdatingStatus ( )
        {
            Status = new StatusServer ( );
            Status.PlayersAmount = 0;
            Update ( );
        }

        /// <summary>
        /// Обновление свойств
        /// во вторичном потоке/фоновом.
        /// </summary>
        async void Update ( )
        {
            await Task.Run ( ( ) =>
            {
                const string uri = "https://api.eveonline.com/Server/ServerStatus.xml.aspx";
                const string currentPath = "eveapi";
                XmlDocument xdocument = new XmlDocument ( );
                XmlNode node;

                while ( true )
                {
                    DateTime cache;

                    xdocument.Load ( uri );

                    node = xdocument.SelectSingleNode ( currentPath + "/result/serverOpen" );
                    Status.IsOpen = Convert.ToBoolean ( node.InnerText );

                    node = xdocument.SelectSingleNode ( currentPath + "/result/onlinePlayers" );
                    Status.PlayersAmount = Convert.ToInt32 ( node.InnerText );

                    node = xdocument.SelectSingleNode ( currentPath + "/cachedUntil" );
                    cache = Convert.ToDateTime ( node.InnerText );

                    //Для этого потока собственное уведомление.
                    OnPropertyChanged ( "Status" );

                    _interval = cache - DateTime.UtcNow;

                    Thread.Sleep ( _interval );
                }
            } );
        }

        #region Члены INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        /// <summary>
        /// Уведомление для xaml,
        /// о том, что свойство изменилось.
        /// </summary>
        /// <param name="name"></param>
        public void OnPropertyChanged ( string name )
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if ( handler != null )
            {
                handler ( this , new PropertyChangedEventArgs ( name ) );
            }
        }
    }
}