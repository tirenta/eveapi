﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace EveApi
{
    abstract class RequestApi
    {
        //Поля
        private int _ApiKey;
        private string _ApiCode;
        private DateTime _cachedUntil;

        //Свойства
        public int ApiKey
        {
            get
            {
                return _ApiKey;
            }
        }
        public string ApiCode
        {
            get
            {
                return _ApiCode;
            }
        }
        /// <summary>
        /// Возвращает время утилизации кеша
        /// из поля
        /// </summary>
        protected DateTime cachedUntil
        {
            get
            {
                return _cachedUntil;
            }
        }
        /// <summary>
        /// Возвращает текущее UTC время
        /// </summary>
        protected DateTime currentTime
        {
            get
            {
                return DateTime.UtcNow;
            }
        }

        //Константы
        private const string _ApiHost = "api.eveonline.com";

        //Конструкторы
        public RequestApi()
        {

        }
        public RequestApi(int key, string code)
        {
            key = _ApiKey;
            code = _ApiCode;
        }

        //Методы
        /// <summary>
        /// Если _ApiKey и _ApiCode пустые
        /// </summary>
        /// <returns>true</returns>
        private bool _isNull()
        {
            if (_ApiKey == 0 & _ApiCode == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //protected bool isValide();
        /// <summary>
        /// Если кеш устарел
        /// currentTime >= _cachedUntil
        /// </summary>
        /// <returns>true</returns>
        protected bool isObsolbete()
        {
            if (currentTime >= _cachedUntil)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Обновляет данные полей
        /// </summary>
        protected void Update()
        {
            string http;
            if (_isNull() == true)
            {
                http = string.Format("https://{0}/{1}/{2}.xml.aspx",
                    _ApiHost, SetLocation(), SetOption());
            }
            else
            {
                http = string.Format("https://{0}/{1}/{2}.xml.aspx?keyID={3}&vCode={4}",
                    _ApiHost, SetLocation(), SetOption(), _ApiKey, _ApiCode);
            }
            XDocument xml = XDocument.Load(http);

            _cachedUntil = Convert.ToDateTime(
                xml.Descendants("cachedUntil").First().Value);

            UpdateData(xml);
        }

        //Абстрактные методы
        protected abstract string SetLocation();
        protected abstract string SetOption();
        protected abstract void UpdateData(XDocument xml);

    }//RequestApi
}