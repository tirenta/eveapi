﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace ChatServiceLib
{
    [ServiceBehavior ( InstanceContextMode = InstanceContextMode.Single ) ]
    public class ChatImplemintation : IChatInterface
    {
        List<IMessageCallBack> callBacks;

        public ChatImplemintation ( )
        {
            callBacks =
                new List<IMessageCallBack> ( );
            Console.WriteLine ( "Конструктор ChatImplemintation построен" );
        }

        #region Члены IChatInterface

        public async void NewMessage ( string user , string message )
        {
            await Task.Run ( ( ) =>
            {
                Console.WriteLine ( "{0}, написал(а): {1}" , user , message );

                if ( callBacks.Count > 0)
                {
                    foreach ( var item in callBacks )
                    {
                        try
                        {
                            item.NewMessageCallBack ( user , message );
                        }
                        catch ( Exception ex )
                        {
                            Console.WriteLine ( "Ошибка: /n{0}", ex.Message );
                            callBacks.Remove ( item );
                        }
                    }
                }
            } );
        }

        public async void NewUser ( string user )
        {
            callBacks.Add ( OperationContext.Current.GetCallbackChannel<IMessageCallBack> ( ) );

            await Task.Run ( ( ) =>
            {
                Console.WriteLine ( "Кто-то подписался/вошел в чат." );

                foreach ( var item in callBacks )
                {
                    item.NewUserCallBack ( user );
                }
            } );
        }

        public async void LeaveUser ( string user )
        {
            callBacks.Remove ( OperationContext.Current.GetCallbackChannel<IMessageCallBack> ( ) );
            await Task.Run ( ( ) =>
            {
                Console.WriteLine ( "Кто-то отписался/вышел из чата" );

                if ( callBacks.Count > 0 )
                {
                    foreach ( var item in callBacks )
                    {
                        item.LeaveUserCallBack ( user );
                    }
                }
            } );
        }

        #endregion
    }
}
