﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Net;
using System.IO;

namespace CyberForum
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = Directory.GetCurrentDirectory();
            string text = File.ReadAllText(string.Format(@"{0}\EulaGet.txt", path));

            string patern = @"<form action=(.+) */>";
            string myNames = @"name=""([a-zA-Z]+)""";
            string myValues = @"value=""([0-9]*\w+\S+[:/.?;=]*)"" ";

            Dictionary<string, string> nValue = new Dictionary<string, string>();

            try
            {
                Match m = Regex.Match(text, patern);

                if (m.Success)
                {
                    Console.WriteLine("Найдено совпадение из: {0}", m.Value);
                    Console.WriteLine("/n{0}", m.Groups[1].Value);

                    try
                    {
                        Regex rgxN = new Regex(myNames);
                        MatchCollection _name = rgxN.Matches(m.Groups[1].Value);

                        if (_name.Count > 0)
                        {
                            Console.WriteLine("{0} совпадений по имени", _name.Count);

                            for (int o = 0; o < _name.Count; ++o)
                            {
                                Console.WriteLine(_name[o].Groups[1].Value);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Коллекция пустая");
                        }
                    }
                    catch (Exception _ex)
                    {
                        Console.WriteLine(_ex.Message);
                    }

                    try
                    {
                        Regex rgxV = new Regex(myValues);
                        MatchCollection _value = rgxV.Matches(m.Groups[1].Value);

                        if (_value.Count > 0)
                        {
                            Console.WriteLine("{0} совпадений по значению", _value.Count);

                            for (int o = 0; o < _value.Count; ++o)
                            {
                                Console.WriteLine(_value[o].Groups[1].Value);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Коллекция пуста");
                        }
                    }
                    catch(Exception __ex)
                    {
                        Console.WriteLine(__ex.Message);
                    }
                }
                else
                {
                    Console.WriteLine("Совпадений не найдено");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();
        }
    }
}
